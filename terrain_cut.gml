/// @desc terrain_cut
/// @param _s_x1
/// @param _s_y1
/// @param _s_x2
/// @param _s_y2
/// @param Instance to cut

var _s_x1 = argument0;
var _s_y1 = argument1;
var _s_x2 = argument2;
var _s_y2 = argument3;
var _poly = argument4.polygon;

var _returnBool = false;
//apply new position to path -> path_shift()
with(argument4) event_user(10);

#region calc intersections


var _len = ds_list_size(_poly);
var _debug = ds_list_create();
var _edgesOnLine = ds_list_create();

for (var _i=0; _i<= _len-2; _i+=2) {
	
	if (_i >= _len - 2) {
		// end to start
		var _multiplier = lines_intersect(_s_x1, _s_y1, _s_x2, _s_y2, _poly[| _i], _poly[| _i+1], _poly[| 0], _poly[| 1], true);	
		
		if (_multiplier != 0) {
			var _x = (_s_x1 + (_s_x2-_s_x1) * _multiplier);
			var _y = (_s_y1 + (_s_y2-_s_y1) * _multiplier);	
		
			if (terrain_get_side(_s_x1, _s_y1, _s_x2, _s_y2, _poly[| _i], _poly[| _i+1]) != terrain_get_side(_s_x1, _s_y1, _s_x2, _s_y2, _poly[| 0], _poly[| 1])) {
				ds_list_insert(_poly, _i + 2,_x);		// add a point(x,y) to polygonlist
				ds_list_insert(_poly, _i + 3 , _y);
				ds_list_add(_edgesOnLine, _i + 2);	// add this point to the on the edge list
				_i += 2;
				_len = ds_list_size(_poly);
				}
			}	
		}else{
		// all points except the last point to the 1st point
		var _multiplier = lines_intersect(_s_x1, _s_y1, _s_x2, _s_y2, _poly[| _i], _poly[| _i+1], _poly[| _i+2], _poly[| _i+3], true);	
		
		if (_multiplier != 0) {
			var _x = (_s_x1 + (_s_x2-_s_x1) * _multiplier);
			var _y = (_s_y1 + (_s_y2-_s_y1) * _multiplier);	
			
			if (terrain_get_side(_s_x1, _s_y1, _s_x2, _s_y2, _poly[| _i], _poly[| _i+1]) != terrain_get_side(_s_x1, _s_y1, _s_x2, _s_y2, _poly[| _i+2], _poly[| _i+3])) {
				ds_list_insert(_poly, _i + 2 ,_x);
				ds_list_insert(_poly, _i + 3 , _y);
				ds_list_add(_edgesOnLine, _i + 2);	
				_i += 2;
				_len = ds_list_size(_poly);
				}
			}
		}
	}
// break if there are no intersections
if (ds_list_size(_edgesOnLine) < 2) {
	ds_list_destroy(_debug);
	ds_list_destroy(_edgesOnLine);
	return false;
	}

#endregion

#region sort intersections (close -> far (vom Start der Linie aus)
var _distances = ds_list_create();
var _map = ds_map_create();

// fill
for (var _i=0; _i<ds_list_size(_edgesOnLine); _i++) {
	_distances[| _i] = abs(point_distance(_poly [| _edgesOnLine[| _i]], _poly[| _edgesOnLine[| _i]+1], _s_x1, _s_y1));
	ds_map_add(_map, _distances[| _i], _edgesOnLine[| _i]);	
	}

// sort
ds_list_sort(_distances, true);
ds_list_clear(_edgesOnLine);

// read sorted points
for (var _i=0; _i<ds_list_size(_distances); _i++) 
	_edgesOnLine[| _i] = ds_map_find_value(_map, _distances[| _i]);

	

#endregion

#region is it a source or a dest point?
var _source = ds_list_create();
var _desti   = ds_list_create();

for (var _i=0; _i< ds_list_size(_edgesOnLine); _i++) {
	var _n1 = 0;
	var _n2 = 0;
	
	// handle last and first index of polygon list extra
	if (_edgesOnLine[| _i] == 0)								_n1 = ds_list_size(_poly);	
	if (_edgesOnLine[| _i] == ds_list_size(_poly)-2)		_n2 = -ds_list_size(_poly);

	// determine side
	var _start = terrain_get_side(_s_x1, _s_y1, _s_x2, _s_y2, _poly[| _edgesOnLine[| _i] - 2 + _n1], _poly[| _edgesOnLine[| _i] - 1 + _n1]);
	var _end = terrain_get_side(_s_x1, _s_y1, _s_x2, _s_y2, _poly[| _edgesOnLine[| _i] + 2 + _n2], _poly[| _edgesOnLine[| _i] + 3 + _n2]);
	
	// is it source or dest?
	if (_start == -1 && _end == 1) {
		//Log("--> Source");
		ds_list_add(_source, _edgesOnLine[| _i]);
		
	}else if (_start == 1 && _end == -1) {
		//Log("--> Dest");
		ds_list_add(_desti, _edgesOnLine[| _i]);
		}	
	}
#endregion

#region reunite Polygons

// break if list have different length
if (ds_list_size(_source) != ds_list_size(_desti)) {Log("exit"); exit;}

var _path = [], _i = 0, _skip = false, _startIndex = noone;
var _list = ds_list_create();
var _array = array_create(_len, false);
var _do_break= 0;	

// for every new polygon
for (var _j=0; _j<ds_list_size(_source)+1; _j++) {
	_path[_j] = path_add();
	_i = 0;
	_startIndex = noone;
	_do_break = 0;
	//Log("J: " +string(_j));
	do {
		// calc start of polygon
		if (_startIndex == noone && _array[_i] == false) _startIndex = _i;
		
		// if it is a start
		if (_startIndex != noone) {
			
			// add points to new polygon
			_array[_i] = true;
			//Log(_i);
			path_add_point(_path[_j], _poly[| _i], _poly[| _i+1], 0);
			
			// jump between dest and source if necessary
			if (!_skip = true) {
				for (var _h=0; _h<ds_list_size(_desti);_h++) {
					if (_desti[| _h] == _i)			{ _i = _source[| _h]-2; _skip = true;   break; }
					if (_source[| _h] == _i)		{ _i = _desti[| _h]-2; _skip = true; 	   break; }
					}
				}else{
				// prevent endless jumping from dest -> soruce -> dest ...
				_skip = false;
				// Schnitte mit TerrainPunkten auffüllen
				terrain_fillLongDistances(_path[_j], path_get_number(_path[_j])-2, global.TERRAIN_POINT_DIST);
				}			
			}
		// nächster Punkt & zurück zum Anfang springen (falls notwendig)
		_i += 2;
		if (_i >= _len) _i = 0; 
		
		if (_do_break++ > 3000) { show_message("BUG: terrain_cut endless do loop..."); break;}
		
		} until (_startIndex == _i);
	}
#endregion

#region neues Terrain ereugen

// Eigenschaften des alten Terrains ermitteln
var _set_phy_density = argument4.phy_density;
var _set_phy_friction = argument4.phy_friction;
var _set_phy_restitution = argument4.phy_restitution;
var _set_isDynamic = argument4.isDynamic;
var _set_start_speedX = argument4.isDynamic ? argument4.start_speedX : 0;
var _set_start_speedY = argument4.isDynamic ? argument4.start_speedY : 0;

// undo -> alte Terrain Instanz in eine Liste innerhalb des obj_logic_cut aufnehmen, damit man sie
//				später wieder aktivieren kann.
ds_list_add(obj_logic_cut.ids_old, argument4);

// gibt an das dieses Terrain zerschnitten wurde -> wird gelöscht, falls man "Take Items" mit false beantwortet
argument4.cuttedTerrain = true;

// deaktivieren oder delete
mapInst_deleteOrDeactivate(argument4, 0);

//Log(array_length_1d(_path));

for (var _i=0; _i<array_length_1d(_path); _i++) {
	var _terrain = terrain_create(_path[_i], global.terrainTexture, _set_phy_density, _set_phy_friction, _set_phy_restitution);
	
	terrain_init(_terrain,
			_set_isDynamic,
			_path[_i],
			_set_phy_density,
			_set_phy_friction,
			_set_phy_restitution,
			_set_start_speedX,
			_set_start_speedY
			);

	path_delete(_path[_i]);
	
	// undo -> neue Terrain Instanz in eine Liste innerhalb des obj_logic_cut aufnehmen, damit man sie
	//				später wieder löschen kann.
	ds_list_add(obj_logic_cut.ids_new, _terrain);
	
	}

#endregion

// Destroy DS
ds_list_destroy(_debug);
ds_list_destroy(_edgesOnLine);
ds_list_destroy(_distances);
ds_list_destroy(_source);
ds_list_destroy(_desti);
ds_list_destroy(_list);
ds_map_destroy(_map)

return true;